# -*- coding: utf-8 -*-

from .client import Client
from .aggregation import Aggregator, timeit

__version__ = "0.1.2"
